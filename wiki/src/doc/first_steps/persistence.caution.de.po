# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2013-10-14 14:12+0300\n"
"PO-Revision-Date: 2015-02-21 22:41-0000\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: Tails Translators <tails-l10n@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<strong>The use of a persistent volume in a system which is designed to provide\n"
"anonymity and leave no trace is a complicated issue.</strong><br/>\n"
"[[Read carefully the warning section.|persistence/warnings]]\n"
msgstr ""
"<strong>Die Verwendung eines beständigen Speicherbereichs auf einem System,\n"
"welches darauf ausgelegt ist, Anonymität zu gewähren und keine Spuren zu\n"
"hinterlassen, ist eine komplizierte Aufgabe.</strong><br/>\n"
"[[Lesen Sie sorgfältig die Warnungshinweise|persistence/warnings]]\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
