# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2016-03-17 15:27+0100\n"
"PO-Revision-Date: 2016-02-19 22:19+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta title=\"Other Linux (Red Hat, Fedora, etc.)\"]] [[!meta stylesheet="
"\"bootstrap\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/"
"stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/overview\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/linux\" rel=\"stylesheet\" title=\"\"]] [[!"
"inline pages=\"install/inc/tails-installation-assistant.inline\" raw=\"yes"
"\"]] [[<span class=\"back\">Back</span>|install/win]]"
msgstr ""
"[[!meta title=\"Andere Linux Systeme (Red Hat, Fedora, usw.)\"]] [[!meta "
"stylesheet=\"bootstrap\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/overview\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"inc/stylesheets/linux\" rel=\"stylesheet\" title=\"\"]] [[!"
"inline pages=\"install/inc/tails-installation-assistant.inline.de\" raw=\"yes"
"\"]] [[<span class=\"back\">Zurück</span>|install/win]]"

#. type: Content of: <h1>
msgid ""
"<strong>Install from Linux</strong> <small>(Red Hat, Fedora, etc.)</small>"
msgstr ""
"<strong>Installation unter Linux</strong> <small>(Red Hat, Fedora, usw.)</"
"small>"

#. type: Content of: outside any tag (error?)
msgid "[[!inline pages=\"install/inc/overview\" raw=\"yes\"]] [["
msgstr "[[!inline pages=\"install/inc/overview.de\" raw=\"yes\"]] [["

#. type: Content of: <div><div><h3>
msgid "Let's go!"
msgstr "Los geht's!"

#. type: Content of: outside any tag (error?)
msgid "|install/linux/usb]]"
msgstr "|install/linux/usb]]"
