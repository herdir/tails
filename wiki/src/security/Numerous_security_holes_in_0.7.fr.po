# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-04-30 17:10-0600\n"
"PO-Revision-Date: 2011-05-01 02:14+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Thu Apr 28 11:12:13 2011\"]]\n"
msgstr "[[!meta date=\"Thu Apr 28 11:12:13 2011\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.7\"]]\n"
msgstr "[[!meta title=\"Nombreux trous de sécurité dans Tails 0.7\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"The following security holes affect Tails 0.7. Some of these allow **remote "
"arbitrary code execution with full administrator rights**, that can be "
"exploited by any attacker with access to the local network."
msgstr ""
"Les trous de sécurité suivants affectent Tails 0.7. Certains de ces trous "
"permettent **l'exécution à distance de code arbitraire avec tous les droits "
"administrateur**, et peuvent être exploités par n'importe quel attaquant "
"ayant accès au réseau local."

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to Tails 0.7.1|news/version_0.7.1]] as "
"soon as possible in case you are still using an older version."
msgstr ""
"Nous vous recommandons **vivement** de faire la [[mise à jour vers Tails "
"0.7.1|news/version_0.7.1]] aussi vite que possible, dans le cas où vous "
"utiliseriez encore une ancienne version."

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "Détails\n"

#. type: Plain text
msgid ""
"These are Debian security announces; details can be found on the [Debian "
"security page](http://security.debian.org/):"
msgstr ""
"Voici les annonces de sécurité de Debian ; des informations plus détaillées "
"sont disponibles sur [la page de sécurité de Debian](http://security.debian."
"org/) :"

#. type: Bullet: '  - '
msgid "x11-xserver-utils (DSA-2213-1)"
msgstr "x11-xserver-utils (DSA-2213-1)"

#. type: Bullet: '  - '
msgid "isc-dhcp (DSA-2216-1)"
msgstr "isc-dhcp (DSA-2216-1)"

#. type: Bullet: '  - '
msgid "libmodplug (DSA-2226-1)"
msgstr "libmodplug (DSA-2226-1)"

#. type: Bullet: '  - '
msgid "openjdk-6 (DSA-2224-1)"
msgstr "openjdk-6 (DSA-2224-1)"
